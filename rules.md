### Basic Job Rules

1. **PMC Mercenaries are allowed to KOS Terrorists and Vice Versa.**
    - Do not KOS the opposing faction in the spawn areas.
2. **Terrorists are NOT allowed to kill anyone else without valid RP reasons. No Terrorist Attacks, etc. However, you may attack Police/Mercenaries in coordinated groups only.**
3. **Terrorists are NOT allowed to run around dropping C4/attempting to explode people/entities/bases without PROPER RP.**
4. **Police are allowed to intervene and arrest or shoot either Mercenaries or Terrorists if they see them fighting.**
5. **For other RP situations all rules apply below. This includes interaction between Terrorists/Mercenaries and Citizens.**
6. **When you die in combat, as a mercenary/terrorist, versus a mercenary/terrorist/police, you cannot re-engage in combat until the full NLR timer is complete.**

### Clarified Common Sense Rules

1. **Use common sense, if it seems like rule-breaking then it probably is. There is a degree of "Common Sense" expected of all players, and if a player lacks this degree of common sense, then they will be banned because they are incapable of playing. This includes Loop-holing.**
2. Upper floors of Nexus is always AOS.
3. Selling jobs is not permitted.
4. Disrespecting is not AOS, however if warned to stop and you persist, you may be arrested.
5. Do not disconnect/suicide to avoid RP, being arrested, etc.
    - If someone disconnects to avoid being arrested, it's disconnecting to avoid an RP situation. It’s like suiciding to avoid an RP situation.
6. Weapons, Drugs (including the equipment to make drugs), and Printers are always illegal.
7. You are NOT permitted to have weapons out. **Police** can attempt to arrest or shoot you depending on the RP situation.
    - POLICE: Do not just KOS a player for having a gun out. You must RP the situation appropriately.

## 1 General Rules

1. Freedom of speech is allowed. Just do not continually harass someone.
2. Do not abuse any exploits, glitches or use hacks. (Please report it for a reward!)
3. Selling/Trading in-game currency is prohibited.
4. Do not link or provide NSFW, malware, or any illicit materials.
5. Do not spam anything. (Voice/Chat/Props/Lockdowns/etc..)
    - Playing music is permitted unless asked to stop.
6. Do not abuse your powers (Voting/Demoting/etc..), this includes revenge demoting.
7. Do not randomly kill a player. (Random Death Match - RDM)
8. Do not randomly kill a player using a vehicle. (Vehicle Death Match - VDM/CDM)
9. Do not randomly attack or interfere with another player.
10. Do not meta-game. (Using information outside of in-character role-play)
    - Recursive Gaming Discord Voice Server is considered in-character.
    - No other 3rd party communication can be considered in-character. (Skype, steam chat, etc..)
11. Follow FearRP (E.g if you are being held at gunpoint).
12. Names above heads, Text-Screens, Advertisements, Door names, Objects clipping through walls/sounds, and Voice Chat are considered in-character.
13. Kill on Sight Areas/Signs/Laws are permitted for your own property only. However, they must be a valid threat.
14. Do not own a building/property if you do not plan to use it. (Unused for over 10 minutes)
15. Do not scam people (Failure to give a weapon/service that they have paid for). This includes gambling with money you don't have.
16. Do not randomly set anything on fire.
17. Do not make false 911 Calls/Reports on other people.
18. Only areas behind owned doors are considered private property, sidewalks and roads are always public.
19. Lock picking / Attempting to break into a home is always arrest-able.
20. Intentionally cross-fire killing is prohibited.
    - If this person is directly shielding the intended target, then this will not be considered RDM.
    - If the person is walking around, into your shot or within unreasonable vicinity of a firefight, it will not be considered RDM.
    - If you indiscriminately shoot/attack into a group or person(s), this **will** be considered RDM.
    - If you use an explosive device on your target that includes a group of innocents, it **will** be considered RDM.
21. Body blocking is not allowed if you're trying to be annoying/minge/troll.
    - Body Blocking is only allowed in the context of Role-playing.
22. Do not mic spam.

## 2 New Life Rules (NLR)

1. Do not return to the spot of your death or affect roleplay around the region for 150 seconds / 2.5 minutes.
    - When you die you are not allowed to remember anyone or thing from your past life.
    - This includes switching jobs.
2. NLR is applied to any death (in which you respawn at spawn, does not include if you are resuscitated).
3. Do not run outside or attempt to abuse the NLR bubble to bypass the NLR system.

## 3 Prop Rules

1. As a civilian, you CANNOT build in the Nexus unless it is for RP raiding purposes. Do not base inside the Nexus!
2. You may spawn a prop to use as cover, as long as its appropriate and done in a RP manner.
3. Invisible/Dark/Hard to see entrances are not permitted.
4. Sliding gates/doors/entrances are not permitted.
5. Hitbox shielding/glitching is not permitted.
6. Head Glitching (Using a defense which completely obscures your head from the view of your attackers while still allowing you to shoot at them) is not permitted.
7. Do not abuse your props. (surfing/climbing/killing/blocking/Using One-way props/Abusive no-colliding)
    - You can build a base as long as it has proper supports. (It has to look like it would be Sturdy/Stable)
8. Do not prop block connecting tunnels/main roads/the underground.
9. You are permitted to prop block within your own home, as long as there is a clear alternative entrance.
    - Do not alter which entrance is blocked.
10. Prop Tunnels are not permitted.
    - No more than 2 turns are permitted in a Prop maze.
    - Crouch entrances cannot exceed 4 seconds to get through.
    - Jump crouch entrances count as a crouch entrance.

## 4 Misc Rules

1. Tow truck driver may tow any car (including government) if illegally parked (left in the street, not in an appropriate parking spot).
    - Do not abuse this. Use common sense or be blacklisted.
    - Towing a government vehicle while they are responding to a crime/911/fire is considered FailRP.
    - Do not tow a car for being unattended for a period of time.
    - You can charge a car owner up to $3,000 to recover their vehicle.
2. Car thefts must always be adverted.
    - You can only hold someone's car for 30 minutes. After 30 minutes, the car owner can use any means (including switching job) to recover their vehicle.
    - You can only charge up to $20,000 to recover a stolen vehicle.
3. Be appropriate on your text-screens.
4. Building text-screens must be valid.
5. If a building has building text-screen, you are not allowed to raid it.
6. Do not impersonate any Moderator or Recursive Gaming staff.
7. Do not disrespect/flame any Moderator or Recursive Gaming staff when OOC. You will be warned/kicked/banned based on severity and number of offenses.
8. You cannot roleplay as a Serial Killer.
9. Do not intentionally attempt to get wanted.
    - For example, sending a 911 call stating there are people with guns at a location when it is you or your group waiting to kill the Police that show up.
10. Do not break roleplay to intentionally interrupt administrative sits. (Physguns, trying to join in, etc.)

## 5 Building Rules

1. The owner of the front door of a building has rights to the entire house.
2. You may build on a roof as long as the roof can be reached by world objects, or a built ramp.
3. Do not build on goverment property or land.
4. Text-screen must follow these requirements.
    - Font size must be over 48pt.
    - Color must not match with the background color, or surrounding colors.
5. Hitbox Glitching/Shielding is not permitted. This includes shooting through one way props, or other props that completely obscures your head from the view of your attackers while still allowing you to shoot at them.
6. Bridges, ladders and ramps.
    - Cannot be fading doors.
    - Must be space for at least two people to stay side by side at once.
    - Must be placed in a realistic way, and supported by other props.
7. Gun dealers are not allowed to base.

## 6 Police / Government Rules

1. You can be corrupt officer, however in doing so must be given a bribe by the offending party.
    - Bribes can be from $300 - $30,000.
2. If you are trying to arrest a fleeing fugitive in a building you may body block the entrance so your fellow officers can make the arrest.
3. You may accept bails, but the amount must be between $300 - $3,000.
4. Government officials must follow traffic laws.
    - They can ignore traffic laws while on emergency calls.
5. All orders are derived from the President.
6. All Police Officers must follow orders of the Police Chief.
7. Do not spam the government chat/radio.
8. You can detain a player for up to 3 minutes without reasoning (you still need a reason for the arrest).
9. SWAT should primarily protect the mayor. Police can be called to also protect him.
10. Civilians cannot protect the Mayor.
11. Do not randomly arrest/warrant/want people.
12. You must have a warrant to enter private property, unless given explicit permission from the owner.
    - You may enter without a warrant if there is probable cause (Wanted inside or hear illegal activities such as gunshots).
    - You cannot warrant someone solely on suspicion.
    - You do not have to send another warrant, if it expires, on the owner of the house if you have not completed the raid yet.
    - Any door within a property can also be warranted.
13. Do not walk around with your issue weapon out unless necessary.
14. Be reasonable with fines/tickets.
15. Do not make useless tickets/fines - You will be blacklisted.
16. The Mayor, Swat Leader and Police Chief can demote anyone underneath them for IC reasons.
17. Do not randomly taze players.
18. The Mayor can not target players (e.g KOS Player) in laws.
19. Police officers can not return to a callout (e.g a bank heist) if they have been killed in the callout.

## 7 Raiding Rules

1. You/your group must enter the building you are raiding within 1 minute of the raid starting.
2. Once you've entered a building to raid it you have 5 minutes to leave.
3. You must ask for a realistic amount. (Up to $5,000).
4. You are not allowed to return to a raid or your base, while a raid is in progress.
5. You must wait 10 minutes between successive raids of the same person/place. This applies to an entire allied group.

## 8 Mugging Rules

1. When mugging a person you must let them know by name. (Advertisements do not count).
2. When mugging, use command "/me" to let the person know.
3. Always follow FearRP (about FearRP [1.11](#fearrp)) when being mugged.
4. You are NOT allowed to shoot the person before you start a mugging.
5. You must wait 10 minutes between successive mugs of the same person/place. This applies to an entire allied group.